## Environment Variables
### DB
This variable represents the connection string to the database. Use the following format: 
`dialect+driver://username:password@host:port/database`

### SECRETKEY
The content of this variable will be used als secret key for Flask Sessions. Choose a random string and keep it secret. 

### LOGINUSER
This variable will be used to set the username of the administrator. Currently there is only one user supported. 

### LOGINPASSWORD
This variable will be used to set the password of the user specified in LOGINUSER