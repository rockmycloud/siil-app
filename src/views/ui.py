from flask import Flask, Blueprint, render_template, request
from flask.helpers import make_response
from auth import login_required
from src import db
from src.models import Category, Question, TFQuestion


ui_blueprint = Blueprint('ui_blueprint', __name__)


@ui_blueprint.route("/", methods=["GET"])
def index():
    return render_template("index.html")


@ui_blueprint.route("/home", methods=["GET"])
@login_required
def home():
    return render_template('home.html')


@ui_blueprint.route("/categories", methods=["GET", "POST"])
@login_required
def categories():
    if request.method == 'POST':
        if 'deleteCategory' in request.form:
            Question.query.filter(Question.categoryID == request.form['inputCategory']).delete(
                synchronize_session=False)
            Category.query.filter(Category.id == request.form['inputCategory']).delete(
                synchronize_session=False)
            db.session.commit()
        else:
            new_category = Category(
                name=request.form['inputCategory'])
            db.session.add(new_category)
            db.session.commit()
    return render_template('categories.html')


@ui_blueprint.route("/questions/<cat>", methods=["GET"])
@login_required
def questions(cat):
    category = Category.query.filter(Category.id == cat).first()

    result_questions = Question.query.filter(
        Question.categoryID == cat).all()
    result_tfquestions = TFQuestion.query.filter(
        TFQuestion.categoryID == cat).all()
    return render_template('questions.html', category=category, questions=result_questions, tfquestions=result_tfquestions)


@ui_blueprint.route("/add-question", methods=["GET", "POST"])
@login_required
def add_question():
    if request.method == 'POST':
        if 'addSCQuestion' in request.form:
            new_question = Question(
                categoryID=request.form['category'],
                type='sc',
                question=request.form['question'],
                right_answer=request.form['rightAnswer'],
                wrong_answer1=request.form['wrongAnswer1'],
                wrong_answer2=request.form['wrongAnswer2'],
                wrong_answer3=request.form['wrongAnswer3'],
                )
            db.session.add(new_question)
            db.session.commit()
        if 'addTFQuestion' in request.form:
            print(request.form)
            if 'tfcheck' in request.form:
                tf_flag = 1
            else:
                tf_flag = 0
            new_question = TFQuestion(
                categoryID=request.form['category'],
                type='tf',
                question=request.form['question'],
                right_answer=tf_flag,
                )
            db.session.add(new_question)
            db.session.commit()
    return render_template('add-question.html')
