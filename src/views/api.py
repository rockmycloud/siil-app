from flask import Flask, Blueprint, request, make_response, jsonify
from src import db
from src.models import Category, Question, TFQuestion
from src.serializer import serialize_category, serialize_question, serialize_tfquestion
import random


api_blueprint = Blueprint('api_blueprint', __name__)


@api_blueprint.route("/categories", methods=["GET"])
def get_categories():
    categories = Category.query.all()
    return make_response(jsonify(serialize_category(categories)), 200)


@api_blueprint.route("/questions", methods=["GET"])
def get_questions():
    questions = serialize_question(Question.query.all())
    questions.extend(serialize_tfquestion(TFQuestion.query.all()))
    return make_response(jsonify(questions), 200)


@api_blueprint.route("/question", methods=["GET"])
def get_question():

    if 'type' in request.args:
        type = request.args.get('type')
    else:
        items = ['sc', 'tf']
        type = random. choice(items)

    if type == 'sc':
        query = db.session.query(Question)
    elif type == 'tf':
        query = db.session.query(TFQuestion)


    if 'category' in request.args:
        category_id = request.args.get('category')

        # Never trust the user. Check if the category id is an integer
        try:
            category_id = int(category_id)
        except Exception as e:
            return make_response(jsonify('Error converting category id.'), 400)

        # Check if a category exists with the given id
        category = Category.query.filter(Category.id == category_id).all()
        if len(category) == 0:
            return make_response(jsonify('Category not found'), 400)
        elif len(category) > 1:
            return make_response(jsonify('Strange things are happened. Multiple categories found.'), 400)

        # Get all questions from the category
        query = query.filter(Question.categoryID==category_id)
    
    # Get a random question from the query result
    rowCount = int(query.count())
    if rowCount == 0:
        return make_response(jsonify('No question found.'), 404)
        
    question = query.offset(int(rowCount*random.random())).first()

    if question.type == 'sc':
        result = serialize_question(question)
    elif question.type == 'tf':
        result = serialize_tfquestion(question)

    return make_response(jsonify(result), 200)
