from src import app
from auth import auth_blueprint
from src.views.ui import ui_blueprint
from src.views.api import api_blueprint
from src.models import Category


app.register_blueprint(auth_blueprint)
app.register_blueprint(ui_blueprint)
app.register_blueprint(api_blueprint, url_prefix="/api")


@app.context_processor
def inject_dict_for_all_templates():
    return dict(categories=Category.query.all())

