def serialize_category(category_list):
    if not isinstance(category_list, list):
        current_object = category_list
        category_list = []
        category_list.append(current_object)

    count = len(category_list)
    if count == 0:
        return []

    result_list = []

    for c in category_list:
        o = {}
        o['id'] = c.id
        o['name'] = c.name
        result_list.append(o)

    return result_list


def serialize_question(question_list):
    if not isinstance(question_list, list):
        current_object = question_list
        question_list = []
        question_list.append(current_object)

    count = len(question_list)
    if count == 0:
        return []

    result_list = []

    for q in question_list:
        o = {}
        o['category_id'] = q.categoryID
        o['category_name'] = q.category.name
        o['type'] = q.type
        o['question'] = q.question
        o['right_answer'] = q.right_answer
        o['wrong_answers'] = [q.wrong_answer1, q.wrong_answer2, q.wrong_answer3]

        result_list.append(o)

    return result_list


def serialize_tfquestion(question_list):
    if not isinstance(question_list, list):
        current_object = question_list
        question_list = []
        question_list.append(current_object)

    count = len(question_list)
    if count == 0:
        return []

    result_list = []

    for q in question_list:
        if q.right_answer == 1:
            answer = True
        else:
            answer = False

        o = {}
        o['category_id'] = q.categoryID
        o['category_name'] = q.category.name
        o['type'] = q.type
        o['question'] = q.question
        o['right_answer'] = answer

        result_list.append(o)

    return result_list
