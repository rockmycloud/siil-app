from flask import Flask, request, render_template, session, url_for, redirect, Blueprint
from functools import wraps
from os import getenv


auth_blueprint = Blueprint('auth_blueprint', __name__)


loginuser = getenv("LOGINUSER")
loginpassword = getenv("LOGINPASSWORD")


def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not session.get('loggedIn'):
            return redirect(url_for('auth_blueprint.login'))
        return func(*args, **kwargs)
    return wrapper


@auth_blueprint.route("/login", methods=['GET'])
def login():
    return render_template('login.html')


@auth_blueprint.route("/login", methods=['POST'])
def login_post():
    if request.form['inputUser'] == loginuser and request.form['inputPassword'] == loginpassword:
        session['loggedIn'] = True
    else:
        return redirect(url_for('ui_blueprint.index'))
    return redirect(url_for('ui_blueprint.home'))
