from src import db, app


class Category(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)


class Question(db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True)
    categoryID = db.Column(db.Integer, db.ForeignKey('category.id'))
    question = db.Column(db.String)
    type = db.Column(db.String)
    right_answer = db.Column(db.String)
    wrong_answer1 = db.Column(db.String)
    wrong_answer2 = db.Column(db.String)
    wrong_answer3 = db.Column(db.String)

    category = db.relationship('Category', foreign_keys='Question.categoryID')


class TFQuestion(db.Model):
    __tablename__ = 'tfquestion'
    id = db.Column(db.Integer, primary_key=True)
    categoryID = db.Column(db.Integer, db.ForeignKey('category.id'))
    question = db.Column(db.String)
    type = db.Column(db.String)
    right_answer = db.Column(db.Integer)

    category = db.relationship('Category', foreign_keys='TFQuestion.categoryID')


with app.app_context():
    db.create_all()